FROM node:14-buster

RUN apt update \
  && apt -y upgrade \ 
  && apt -y install libgl1-mesa-glx libgl1-mesa-dev python3 python3-pip rsync \
  && npm install -g gatsby-cli \
  && pip3 install awscli \
  && mkdir /opt/s3deploy \
  && wget -O /s3deploy.tar.gz 'https://github.com/bep/s3deploy/releases/download/v2.3.5/s3deploy_2.3.5_Linux-64bit.tar.gz' \
  && tar zxvf /s3deploy.tar.gz -C /opt/s3deploy/ \
  && chmod +x /opt/s3deploy \
  && rm -f /s3deploy.tar.gz \
  && ln -s /opt/s3deploy/s3deploy /usr/local/bin/ \
  && wget -O env-aws.zip 'https://github.com/Droplr/aws-env/archive/refs/tags/v0.4.zip' \
  && unzip -jo env-aws.zip aws-env-0.4/bin/aws-env-linux-amd64 \
  && rm env-aws.zip \
  && chmod +x aws-env-linux-amd64 \
  && mv aws-env-linux-amd64 /usr/local/bin/aws-env \
  && apt-get -y clean \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /app
